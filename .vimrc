set guifont=Monospace\ 12

set number

set tabstop=4
set shiftwidth=4
set softtabstop=4 " Used when backspacing on indentation.
set smartindent
set expandtab

colorscheme evening

syntax on

set hlsearch

set listchars=tab:»·,trail:·
set list

nnoremap <M-j> 5j
nnoremap <M-k> 5k

nnoremap \\ $

inoremap éj <Esc>
inoremap öj <Esc>
inoremap ;j <Esc>

nnoremap gs :w<Return>
nnoremap dq :bd!<Return>
nnoremap q<Tab> :qa!<Return>
nnoremap q<Space> :qa!<Return>
nnoremap ZQ :qw!<Return>

vnoremap q<Tab> <Esc>:qa!<Return>
vnoremap q<Space> <Esc>:qa!<Return>

" Use double slash at the end to tell vim to make sure files from different
" locations with the same name have a unique name under the target directory.
set directory=~/.vim/swapfiles//

set undofile
set undodir=~/.vim/undofiles//

set hidden
nnoremap <F5> :bp<Return>
nnoremap <F6> :bn<Return>
nnoremap <M-2> :bp<Return>
nnoremap <M-3> :bn<Return>
nnoremap <Space>j :bp<Return>
nnoremap <Space>k :bn<Return>
